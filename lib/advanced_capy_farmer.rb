require 'yaml'
require 'CapyFarmer'

CapyFarmer::Create.config
CapyFarmer::Create.write_structure
CapyFarmer::Create.write_files
