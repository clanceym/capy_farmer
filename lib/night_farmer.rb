# require "capy_farmer/version"
require 'fileutils'
require 'yaml'

module NightFarmer
  module Create
    def self.config(project = nil, name = nil)
      if project
        @@project = project
      else
        print "Project name: "
        STDOUT.flush
        @@project = gets.chomp
      end

      if name
        @@name = name
      else
        print "Your name: "
        STDOUT.flush
        @@name = gets.chomp
      end

      if project && name
        puts "Going to create project #{@@project} for #{@@name}"
      else
        print "Going to create project #{@@project} for #{@@name}: (Y/N) "
        STDOUT.flush
        continue = gets.chomp

        unless continue.upcase == 'Y'
          puts "Ok, try again"
          exit
        end
      end
    end

    def self.write_structure
      FileUtils.mkpath "./#{@@project}/nightwatch/bin"
      FileUtils.mkpath "./#{@@project}/nightwatch/notes"
      FileUtils.mkpath "./#{@@project}/nightwatch/pages"
      FileUtils.mkpath "./#{@@project}/nightwatch/reports"
      FileUtils.mkpath "./#{@@project}/nightwatch/tests"
      FileUtils.touch "./#{@@project}/nightwatch/nightwatch.js"
      FileUtils.touch "./#{@@project}/nightwatch/nightwatch.json"
      FileUtils.touch "./#{@@project}/nightwatch/pages/home_page.rb"
      FileUtils.touch "./#{@@project}/nightwatch/pages/login_page.rb"
      FileUtils.touch "./#{@@project}/nightwatch/README.md"
      FileUtils.touch "./#{@@project}/nightwatch/tests/login_test.js"
      puts "Done writing folder structure"
    end

    def self.write_files
      write_nightwatch_js
      write_nightwatch_json
      write_test('login')
      write_page('login')
      write_readme
      puts "Done writing files. Enjoy!"
    end

    def self.write_nightwatch_js
      open("./#{@@project}/nightwatch/nightwatch.js", 'a') { |f|
        f << "require('nightwatch/bin/runner.js');"
      }
    end

      def self.write_nightwatch_json
        open("./#{@@project}/nightwatch/nightwatch.json", 'a') { |f|
          f << "{\n"
          f << "  \"src_folders\" : [\"tests\"],\n"
          f << "  \"output_folder\" : \"reports\",\n"
          f << "  \"custom_commands_path\" : \"\",\n"
          f << "  \"custom_assertions_path\" : \"\",\n"
          f << "  \"page_objects_path\" : \"pages\",\n"
          f << "  \"globals_path\" : \"\",\n"
          f << "\n"
          f << "  \"selenium\" : {\n"
          f << "    \"start_process\" : false,\n"
          f << "    \"server_path\" : \"\",\n"
          f << "    \"log_path\" : \"\",\n"
          f << "    \"host\" : \"127.0.0.1\",\n"
          f << "    \"port\" : 4444,\n"
          f << "    \"cli_args\" : {\n"
          f << "      \"webdriver.chrome.driver\" : \"\",\n"
          f << "      \"webdriver.ie.driver\" : \"\"\n"
          f << "    }\n"
          f << "  },\n"
          f << "\n"
          f << "  \"test_settings\" : {\n"
          f << "    \"default\" : {\n"
          f << "      \"launch_url\" : \"http://localhost\",\n"
          f << "      \"selenium_port\"  : 4444,\n"
          f << "      \"selenium_host\"  : \"localhost\",\n"
          f << "      \"silent\": true,\n"
          f << "      \"screenshots\" : {\n"
          f << "        \"enabled\" : false,\n"
          f << "        \"path\" : \"\"\n"
          f << "      },\n"
          f << "      \"desiredCapabilities\": {\n"
          f << "        \"browserName\": \"firefox\",\n"
          f << "        \"javascriptEnabled\": true,\n"
          f << "        \"acceptSslCerts\": true\n"
          f << "      }\n"
          f << "    },\n"
          f << "    \"chrome\" : {\n"
          f << "      \"desiredCapabilities\": {\n"
          f << "        \"browserName\": \"chrome\",\n"
          f << "        \"javascriptEnabled\": true,\n"
          f << "        \"acceptSslCerts\": true\n"
          f << "      }\n"
          f << "    },\n"
          f << "    \"phantom\" : {\n"
          f << "      \"desiredCapabilities\" : {\n"
          f << "        \"browserName\" : \"phantomjs\",\n"
          f << "        \"javascriptEnabled\" : true,\n"
          f << "        \"acceptSslCerts\" : true,\n"
          f << "        \"phantomjs.binary.path\" : \"phantomjs.exe\",\n"
          f << "        \"phantomjs.cli.args\" : [\"--version\"]\n"
          f << "      }\n"
          f << "    }\n"
          f << "  }\n"
          f << "}\n"
          f << "\n"
        }
      end

      def self.write_page(page)
        open("./#{@@project}/nightwatch/pages/#{page}_page.rb", 'a') { |f|
          f << "module.exports = {\n"
          f << "  url: 'https://www.vintriidci.com/#/login',\n"
          f << "  elements: {\n"
          f << "    userName: {\n"
          f << "      selector: 'input[id=input-type-email]'\n"
          f << "    },\n"
          f << "    password: {\n"
          f << "      selector: 'input[id=password]'\n"
          f << "    },\n"
          f << "    signIn: {\n"
          f << "      selector: 'button[text=SignIn]'\n"
          f << "    },\n"
          f << "    crumbTrail: {\n"
          f << "      selector: '#header > div.breadcrumb-container.col-lg-12.ng-scope'\n"
          f << "    },\n"
          f << "    myProjectsTitle: {\n"
          f << "      selector: '#vintri-app > ui-view > div.container-fluid.ng-scope > div > div:nth-child(1) > div.col-sm-6.col-lg-4.my-projects > h3 > span'\n"
          f << "    }\n"
          f << "  }\n"
          f << "};\n"
          f << "\n"
        }
      end

      def self.write_test(test)
        open("./#{@@project}/nightwatch/tests/#{test}_test.js", 'a') { |f|
          f << "module.exports = {\n"
          f << "  '@disabled': true, // This will prevent the test module from running.\n"
          f << "\n"
          f << "  'Demo test Google' : function (browser) {\n"
          f << "    browser\n"
          f << "      .url('http://www.google.com')\n"
          f << "      .waitForElementVisible('body', 1000)\n"
          f << "      .setValue('input[type=text]', 'nightwatch')\n"
          f << "      .waitForElementVisible('button[name=btnG]', 1000)\n"
          f << "      .click('button[name=btnG]')\n"
          f << "      .pause(1000)\n"
          f << "      .assert.containsText('#main', 'Night Watch')\n"
          f << "      .end();\n"
          f << "  }\n"
          f << "};\n"
          f << "\n"
        }
      end

      def self.readme(test)
        open("./#{@@project}/nightwatch/README.md", 'a') { |f|
          f << "#{@@project} Automated Functional Test Suite\n"
          f << "======\n"
          f << "\n"
          f << "This is the suite of tests for testing #{@@project} using\n"
          f << "- [ nightwatch.js ](http://nightwatchjs.org/)\n"
          f << "- [ phantom.js ](http://phantomjs.org/)\n"
          f << "- [ node.js ](https://nodejs.org)\n"
          f << "\n"
          f << "Setup\n"
          f << "-----\n"
          f << "\n"
          f << "- [ Quick Start ](https://github.com/nightwatchjs/nightwatch/wiki/Windows-Quick-Start)\n"
          f << "\n"
        }
      end
    end
  end
end

if __FILE__==$0
  NightFarmer::Create.config
  NightFarmer::Create.write_structure
  NightFarmer::Create.write_files
end
