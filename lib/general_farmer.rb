require_relative 'capy_farmer.rb'
require_relative 'night_farmer.rb'

print "Project name: "
STDOUT.flush
project = gets.chomp

print "Your name: "
STDOUT.flush
name = gets.chomp

CapyFarmer::Create.config(project, name)
CapyFarmer::Create.write_structure
CapyFarmer::Create.write_files

NightFarmer::Create.config(project, name)
NightFarmer::Create.write_structure
NightFarmer::Create.write_files
