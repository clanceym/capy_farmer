# require "capy_farmer/version"
require 'fileutils'
require 'yaml'

module CapyFarmer
  module Create
    def self.config(project = nil, name = nil)
      if project
        @@project = project
      else
        print "Project name: "
        STDOUT.flush
        @@project = gets.chomp
      end

      if name
        @@name = name
      else
        print "Your name: "
        STDOUT.flush
        @@name = gets.chomp
      end

      if project && name
        puts "Going to create project #{@@project} for #{@@name}"
      else
        print "Going to create project #{@@project} for #{@@name}: (Y/N) "
        STDOUT.flush
        continue = gets.chomp

        unless continue.upcase == 'Y'
          puts "Ok, try again"
          exit
        end
      end

    end

    def self.write_structure
      FileUtils.mkpath "./#{@@project}/capybara/features"
      FileUtils.mkpath "./#{@@project}/capybara/features/pages"
      FileUtils.mkpath "./#{@@project}/capybara/features/reports"
      FileUtils.mkpath "./#{@@project}/capybara/features/screenshots"
      FileUtils.mkpath "./#{@@project}/capybara/features/step_definitions"
      FileUtils.mkpath "./#{@@project}/capybara/features/support/config/data"
      FileUtils.mkpath "./#{@@project}/capybara/notes"
      FileUtils.mkpath "./#{@@project}/capybara/reports"
      FileUtils.touch "./#{@@project}/capybara/cucmber.yml"
      FileUtils.touch "./#{@@project}/capybara/features/login.feature"
      FileUtils.touch "./#{@@project}/capybara/features/logout.feature"
      FileUtils.touch "./#{@@project}/capybara/features/pages/home_page.rb"
      FileUtils.touch "./#{@@project}/capybara/features/pages/login_page.rb"
      FileUtils.touch "./#{@@project}/capybara/features/step_definitions/login_steps.rb"
      FileUtils.touch "./#{@@project}/capybara/features/step_definitions/logout_steps.rb"
      FileUtils.touch "./#{@@project}/capybara/features/support/config/data/default.yml"
      FileUtils.touch "./#{@@project}/capybara/features/support/env.rb"
      FileUtils.touch "./#{@@project}/capybara/features/support/hooks.rb"
      FileUtils.touch "./#{@@project}/capybara/Gemfile"
      FileUtils.touch "./#{@@project}/capybara/Rakefile"
      FileUtils.touch "./#{@@project}/capybara/README.md"
      puts "Done writing folder structure"
    end


      def self.write_files
        write_cucmber_yml
        write_default_yml
        write_env
        write_feature('login')
        write_feature('logout')
        write_gemfile
        write_hooks
        write_page('login')
        write_page('home')
        write_page('logout')
        write_rakefile
        write_readme
        write_step('login')
        write_step('logout')
        puts "Done writing files. Enjoy!"
      end

      def self.write_cucmber_yml
        template = { 'default' => '--snippet-type percent --format html --out=reports\<%= Time.new.strftime("%Y_%m_%d_%H-%M-%S") %>.html' }
        open("./#{@@project}/capybara/cucmber.yml", 'a') { |f|
          f.write template.to_yaml
        }
      end

      def self.write_feature(feature)
        open("./#{@@project}/capybara/features/#{feature}.feature", 'a') { |f|
          f << "Feature: Basic #{feature}\n"
          f << "  In order to \n"
          f << "  As an \n"
          f << "  I want to \n"
          f << "\n"
          f << "Scenario: #{feature} happy path\n"
          f << "  Given \n"
          f << "   When \n"
          f << "   Then \n"
          f << "\n"
        }
      end

      def self.write_page(page)
        open("./#{@@project}/capybara/features/pages/#{page}_page.rb", 'a') { |f|
          f << "##\n"
          f << "# Page class that defines the #{page} page \n"
          f << "# Based on site_prism[https://github.com/natritmeyer/site_prism]\n"
          f << "# Style guide[https://github.com/bbatsov/ruby-style-guide]\n"
          f << "#\n"
          f << "# Author::    #{@@name}\n"
          f << "# Copyright:: Copyright (c) #{Time.now.strftime("%Y")}\n"
          f << "#\n"
          f << "##\n"
          f << "class #{page.capitalize}Page < SitePrism::Page\n"
          f << "  # The URL of the page. May contain parameters i.e. https://site.x/{/username} or \"/search{?query*}\" see addressable[https://github.com/sporkmonger/addressable]\n"
          f << "  set_url  \"/#{page}\"\n"
          f << "  # set_url_matcher # Matcher if needed\n"
          f << "\n"
          f << "  # Page elements section. See site_prism elements[https://github.com/natritmeyer/site_prism#elements]\n"
          f << "  # The element method takes 2 arguments: the name of the element as a symbol, and a css selector as a string.\n"
          f << "  #\n"
          f << "  # If you define the following element\n"
          f << "  #   element :search_field, \"input[name='q']\"\n"
          f << "  #\n"
          f << "  # The following will automatically be created for each element defined\n"
          f << "  #   PageClass.search_field\n"
          f << "  #   PageClass.has_search_field?\n"
          f << "  #   PageClass.has_no_search_field?\n"
          f << "  #   PageClass.wait_for_search_field\n"
          f << "  #   PageClass.wait_for_search_field(10)\n"
          f << "  #   PageClass.wait_until_search_field_visible\n"
          f << "  #   PageClass.wait_until_search_field_visible(10)\n"
          f << "  #   PageClass.wait_until_search_field_invisible\n"
          f << "  #   PageClass.wait_until_search_field_invisible(10)\n"
          f << "  #   PageClass.all_there? # This one checks that all defined/mapped elements exist on the page\n"
          f << "  element :element_name, 'css_xpath_selector'\n"
          f << "  \n"
          f << "end\n"
        }
      end

      def self.write_step(step)
        open("./#{@@project}/capybara/features/step_definitions/#{step}_steps.rb", 'a') { |f|
          f << "include DataMagic\n"
          f << "\n"
          f << "Given %r{^an invalid user$} do\n"
          f << "  @#{step} = #{step.capitalize}Page.new\n"
          f << "  @#{step}.load\n"
          f << "  @username = 'joe'\n"
          f << "  @password = 'joe'\n"
          f << "end\n"
          f << "\n"
          f << "When %r{^that user attempts to log into the application$} do\n"
          f << "  @#{step}.username.set @username\n"
          f << "  @#{step}.password.set @password\n"
          f << "  @#{step}.sign_in.click\n"
          f << "end\n"
          f << "\n"
          f << "Then %r{^the user (should|should not) be able to log in$} do |should_or_not|\n"
          f << "  if should_or_not == 'should'\n"
          f << "    expect(page).to have_content('#{@@project}')\n"
          f << "  else\n"
          f << "    page.should have_no_content('#{@@project}')\n"
          f << "  end\n"
          f << "end\n"
          f << "\n"
        }
      end

      def self.write_default_yml
        template = {"valid_login"=>
                      {"username"=>"valid@user", "password"=>"!password"},
                    "invalid_login"=>
                      {"username"=>"~first_name", "password"=>"~characters(10)"},
                    "project_details"=>
                      {:project_name=>"#{@@project}", :project_number=>"001"}}
        open("./#{@@project}/capybara/features/support/config/data/default.yml", 'a') { |f|
          f.write template.to_yaml
        }
      end

      def self.write_env
        open("./#{@@project}/capybara/features/support/env.rb", 'a') { |f|
          f << "# encoding: utf-8\n"
          f << "require 'capybara'\n"
          f << "require 'capybara/cucumber'\n"
          f << "require 'capybara/poltergeist'\n"
          f << "require 'capybara/rspec'\n"
          f << "require 'pry'\n"
          f << "require 'rspec/expectations'\n"
          f << "require 'site_prism'\n"
          f << "require 'launchy'\n"
          f << "require 'data_magic'\n"
          f << "\n"
          f << "Capybara.run_server              = false\n"
          f << "Capybara.default_driver          = :poltergeist\n"
          f << "Capybara.default_max_wait_time   = 30\n"
          f << "Capybara.app_host                = \"https:\\\\#{@@project}{ENV['vi_env']}.domain.com/#\"\n"
          f << "Capybara.save_and_open_page_path = './#{@@project}/capybara/features/screenshots'\n"
          f << "\n"
          f << "DataMagic.yml_directory = File.join(File.dirname(__FILE__), 'config', 'data')\n"
          f << "\n"
        }
      end

      def self.write_hooks
        open("./#{@@project}/capybara/features/support/hooks.rb", 'a') { |f|
          f << "$scenario_times = {}\n"
          f << "\n"
          f << "Around do |scenario, block|\n"
          f << "  start = Time.now\n"
          f << "  block.call\n"
          f << "  if scenario.failed?\n"
          f << "    scen_status = 'F'\n"
          f << "  else\n"
          f << "    scen_status = 'P'\n"
          f << "  end\n"
          f << "  $scenario_times[\"\#{scen_status} : \#{scenario.feature.file}::\#{scenario.name}\"] = Time.now - start\n"
          f << "end\n"
          f << "\n"
          f << "After do |scenario|\n"
          f << "  # Do something after each scenario.\n"
          f << "  # The +scenario+ argument is optional, but\n"
          f << "  # if you use it, you can inspect status with\n"
          f << "  # the #failed?, #passed? and #exception methods.\n"
          f << "  save_and_open_screenshot if ENV['vi_debug']\n"
          f << "\n"
          f << "  if scenario.failed?\n"
          f << "    if ENV['vi_debug']\n"
          f << "      save_and_open_screenshot\n"
          f << "      binding.pry\n"
          f << "      pass\n"
          f << "    else\n"
          f << "      # Tell Cucumber to quit after this scenario is done - if it failed.\n"
          f << "      Cucumber.wants_to_quit = true\n"
          f << "    end\n"
          f << "  end\n"
          f << "end\n"
          f << "at_exit do |scenario|\n"
          f << "  out = \"\#{Time.new.strftime(\"%Y_%m_%d_%H-%M-%S\")}\n\"\n"
          f << "  puts \"\n\t#{'-' * 50}\n\t|\t\tScenario Timing\n\t\#{'-' * 50}\"\n"
          f << "  $scenario_times.each do |key, value|\n"
          f << "    puts \"\t| \#{value.round(2)}\#{' ' * (10 - (value.round(2)).to_s.length)} : \#{key.gsub(/\s+\./,'')}\"\n"
          f << "    out << \"\#{value.round(2)}\#{' ' * (10 - (value.round(2)).to_s.length)} : \#{key.gsub(/\s+\./,'')}\n\"\n"
          f << "  end\n"
          f << "  open(\"reports\\\#{Time.new.strftime(\"%Y_%m_%d_%H-%M-%S\")}_performance.log\", 'a') { |f| f.write out }\n"
          f << "\n"
          f << "end\n"
          f << "\n"
        }
      end

      def self.write_gemfile
        open("./#{@@project}/capybara/gemfile", 'a') { |f|
          f << "source 'https://rubygems.org'\n"
          f << "gem 'cucumber'\n"
          f << "gem 'capybara'\n"
          f << "gem 'poltergeist'\n"
          f << "gem 'site_prism'\n"
          f << "gem 'data_magic'\n"
          f << "gem 'slowhandcuke'\n"
          f << "gem 'pry'\n"
          f << "gem 'rspec'\n"
          f << "gem 'launchy'\n"
          f << "\n"
        }
      end

      def self.write_rakefile
        open("./#{@@project}/capybara/rakefile", 'a') { |f|
          f << "require 'rubygems'\n"
          f << "require 'cucumber'\n"
          f << "require 'cucumber/rake/task'\n"
          f << "require 'features/support/data_setup.rb'\n"
          f << "begin; require 'parallel_tests/tasks'; rescue LoadError; end\n"
          f << "\n"
          f << "task :default => :features\n"
          f << "\n"
          f << "Cucumber::Rake::Task.new(:features) { |t| t.profile = 'default' }\n"
          f << "\n"
          f << "task :setup_data do\n"
          f << "  # Get a unique run identifier\n"
          f << "  # Get user specific info\n"
          f << "  # Create run file from template\n"
          f << "  # Show run file to user\n"
          f << "end\n"
          f << "\n"
        }
      end

      def self.write_readme
        open("./#{@@project}/capybara/README.md", 'a') { |f|
          f << "#{@@project} Automated Functional Test Suite\n"
          f << "======\n"
          f << "\n"
          f << "This is the suite of tests for testing #{@@project} using\n"
          f << "- [ capybara ](https://github.com/jnicklas/capybara)\n"
          f << "- [ poltergeist ](https://github.com/teampoltergeist/poltergeist)\n"
          f << "- [ site_prism ](https://github.com/natritmeyer/site_prism)\n"
          f << "\n"
          f << "Setup\n"
          f << "-----\n"
          f << "\n"
          f << "Make sure you have ruby 1.9.3 (latest) installed, as well as the bundler gem. Run `bundle install` from this directory.\n"
          f << "\n"
          f << "To use `rake` you will need to set your environment variable to the target test environment (ci, demo) using `set vi_env=ci`.\n"
          f << "\n"
          f << "rake commands\n"
          f << "-------------\n"
          f << "\n"
          f << "The rake profiles available are `features`, `focus`, and `setup`.\n"
          f << "\n"
          f << "1.  `features` is the default and runs all features except for setup features.\n"
          f << "2.  `focus` runs the feature(s) tagged with `@focus`. Use this when you are testing a feature.\n"
          f << "3.  `setup` runs the setup features for getting an environment ready after a database wipe.\n"
          f << "\n"
          f << "To run the ID Cap tests\n"
          f << "-----------------------\n"
          f << "\n"
          f << "1.  Open Ansicon (~path_to\ansicon.exe)\n"
          f << "2.  `cd ~path_to\#{@@project}`\n"
          f << "3.  `???`\n"
          f << "\n"
        }
      end
    end
  end
end

if __FILE__==$0
  CapyFarmer::Create.config
  CapyFarmer::Create.write_structure
  CapyFarmer::Create.write_files
end
