# CapyFarmer

CapyFarmer helps to setup a functional test suit quickly so you don't need to remember what goes where.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'capy_farmer'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install capy_farmer

## Usage
### Basic

For now, run lib\capy_farmer and enter the project name you would like, along with your own name.

### Advanced

Open the project_seed.yml file and fill the information required.
```yaml
---
project_seed:
  :project_name: This is the name of the project, and will be used to create the root folder
  :application_name: This is the name of the application that you are testing
  :author: The author of the tests (your name)
  :base_url: This is the base url for your application. Enter this like http://<env>.application.domain.com where <env> is the test environment like qa, rc, ga, beta, alpha, etc.
  :login_page: This is the bit after the .com for the login page. i.e. /#/login
  :login_page_username_field_id: This can be an id in the form of \#id or a full CSS selector path
  :login_page_password_field_id: This can be an id in the form of \#id or a full CSS selector path
  :login_page_sign_in_button_id: This can be an id in the form of \#id or a full CSS selector path
  :home_page: This is the bit after the .com for the login page. i.e. /#/home
  :logout_button_id: This is the bit after the .com for the login page. i.e. /#/login
```
Then run lib\advanced_capy_farmer.

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/clanceym/capy_farmer/capy_farmer.

